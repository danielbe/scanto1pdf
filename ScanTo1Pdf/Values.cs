﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ScanTo1Pdf
{
    public static class Values
    {
        private static readonly char DriveLetter = Assembly.GetEntryAssembly().Location[0];

        private static string configurationPath = 
            $@"{DriveLetter}:\users\{Help.GetUserName()}\AppData\Roaming\Tastscharf\";
        public static string ConfigurationPath
        { 
            get
            {
                return configurationPath;
            }
            set
            {
                configurationPath = value;
                ConfigurationFile = configurationPath + "tastscharf";
            }
        }

        public static string ConfigurationFile { get; set; } = configurationPath + "tastscharf";

        public static string DesktopDefaultPath = 
            $@"{DriveLetter}:\users\{Help.GetUserName()}\Desktop\";

        public static string TempImgPath { get; set; }  = "";
        public static string FinalPdfPath { get; set; } = "";
        public static bool DeleteFiles { get; set; }
    }
}
