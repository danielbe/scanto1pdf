﻿using Org.BouncyCastle.Asn1.Crmf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScanTo1Pdf
{
    public static class ConfigurationFile
    {
        public static void Read()
        {
            string[] content = File.ReadAllLines(Values.ConfigurationFile);
            foreach(string s in content)
            {
                if (s.StartsWith("pdf="))
                    Values.FinalPdfPath = s.Substring(4);
                else if (s.StartsWith("img="))
                    Values.TempImgPath = s.Substring(4);
                else if (s.StartsWith("del="))
                    Values.DeleteFiles = s.Substring(4).Equals("true");
            }
        }

        public static void Write()
        {
            File.WriteAllText(Values.ConfigurationFile,
                "pdf=" + Values.FinalPdfPath + Environment.NewLine +
                "img=" + Values.TempImgPath + Environment.NewLine +
                "del=" + Values.DeleteFiles.ToString());
        }

        public static string Create()
        {
            string returnMsg = "";
            try
            {
                if (!Directory.Exists(Values.ConfigurationPath))
                    Directory.CreateDirectory(Values.ConfigurationPath);
            }
            catch (Exception)
            {
                returnMsg = Texts.ConfiguationFileError;
                Values.ConfigurationPath = "";
            }
            File.WriteAllText(Values.ConfigurationFile,
                "pdf=" + Values.DesktopDefaultPath + Environment.NewLine +
                "img=" + Values.DesktopDefaultPath + Environment.NewLine +
                "del=true");

            return returnMsg;
        }
    }
}
