﻿using System;
using System.Windows.Forms;
using System.Reflection;
using System.IO;

namespace ScanTo1Pdf
{
    static class Help
    {
        public static string GetUserName()
        {
            return Environment.UserName;
        }

        public static string GetDateTime()
        {
            string dateTime = DateTime.Now.ToString().Trim().Replace(":", ".");
            return dateTime;
        }

        public static void CreateSettingsFile(string temp, string final, string del)
        {

            string write = "tempImgPath=" +  temp + Environment.NewLine +
                "finalPdfPath=" + final + Environment.NewLine +
                "deleteImgAfterPdf=" + del;
            
            //File.WriteAllLines
        }

        public static void ResetSettingsFile()
        {
            CreateSettingsFile(Values.TempImgPath, Values.FinalPdfPath, "true");
            ResetPaths();
        }

        public static void ResetPaths()
        {
            Values.FinalPdfPath = Values.DesktopDefaultPath;
            Values.TempImgPath = Values.DesktopDefaultPath;
        }
    }
}
