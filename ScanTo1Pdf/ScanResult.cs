﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScanTo1Pdf
{
    public class ScanResult
    {
        public string Path { get; set; }
        public bool Success { get; set; } = true;
        public string ErrorMessage { get; set; }
    }
}
