﻿namespace ScanTo1Pdf
{
    partial class FormSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ControlAccept = new System.Windows.Forms.Button();
            this.ControlImagePath = new System.Windows.Forms.TextBox();
            this.ControlPdfPath = new System.Windows.Forms.TextBox();
            this.savePathImage = new System.Windows.Forms.Label();
            this.savePathPdf = new System.Windows.Forms.Label();
            this.info = new System.Windows.Forms.Label();
            this.ControlDeleteFiles = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ControlAccept
            // 
            this.ControlAccept.Location = new System.Drawing.Point(16, 187);
            this.ControlAccept.Name = "ControlAccept";
            this.ControlAccept.Size = new System.Drawing.Size(256, 23);
            this.ControlAccept.TabIndex = 0;
            this.ControlAccept.Text = "Übernehmen";
            this.ControlAccept.UseVisualStyleBackColor = true;
            this.ControlAccept.Click += new System.EventHandler(this.ControlAccept_Click);
            // 
            // ControlImagePath
            // 
            this.ControlImagePath.Location = new System.Drawing.Point(13, 13);
            this.ControlImagePath.Name = "ControlImagePath";
            this.ControlImagePath.Size = new System.Drawing.Size(259, 20);
            this.ControlImagePath.TabIndex = 1;
            // 
            // ControlPdfPath
            // 
            this.ControlPdfPath.Location = new System.Drawing.Point(13, 75);
            this.ControlPdfPath.Name = "ControlPdfPath";
            this.ControlPdfPath.Size = new System.Drawing.Size(259, 20);
            this.ControlPdfPath.TabIndex = 2;
            // 
            // savePathImage
            // 
            this.savePathImage.AutoSize = true;
            this.savePathImage.Location = new System.Drawing.Point(13, 36);
            this.savePathImage.Name = "savePathImage";
            this.savePathImage.Size = new System.Drawing.Size(212, 13);
            this.savePathImage.TabIndex = 3;
            this.savePathImage.Text = "Zwischenspeicherort für eingescanntes Bild";
            // 
            // savePathPdf
            // 
            this.savePathPdf.AutoSize = true;
            this.savePathPdf.Location = new System.Drawing.Point(13, 98);
            this.savePathPdf.Name = "savePathPdf";
            this.savePathPdf.Size = new System.Drawing.Size(100, 13);
            this.savePathPdf.TabIndex = 4;
            this.savePathPdf.Text = "Speicherort für PDF";
            // 
            // info
            // 
            this.info.AutoSize = true;
            this.info.Location = new System.Drawing.Point(10, 110);
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(93, 13);
            this.info.TabIndex = 5;
            this.info.Text = "Ohne Dateinamen";
            // 
            // ControlDeleteFiles
            // 
            this.ControlDeleteFiles.AutoSize = true;
            this.ControlDeleteFiles.Location = new System.Drawing.Point(13, 142);
            this.ControlDeleteFiles.Name = "ControlDeleteFiles";
            this.ControlDeleteFiles.Size = new System.Drawing.Size(267, 17);
            this.ControlDeleteFiles.TabIndex = 6;
            this.ControlDeleteFiles.Text = "Gescannte Bilder nach Erstellung des PDF löschen";
            this.ControlDeleteFiles.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Ohne Dateinamen";
            // 
            // FormSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 265);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ControlDeleteFiles);
            this.Controls.Add(this.info);
            this.Controls.Add(this.savePathPdf);
            this.Controls.Add(this.savePathImage);
            this.Controls.Add(this.ControlPdfPath);
            this.Controls.Add(this.ControlImagePath);
            this.Controls.Add(this.ControlAccept);
            this.Name = "FormSettings";
            this.Text = "FormSettings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ControlAccept;
        private System.Windows.Forms.TextBox ControlImagePath;
        private System.Windows.Forms.TextBox ControlPdfPath;
        private System.Windows.Forms.Label savePathImage;
        private System.Windows.Forms.Label savePathPdf;
        private System.Windows.Forms.Label info;
        private System.Windows.Forms.CheckBox ControlDeleteFiles;
        private System.Windows.Forms.Label label1;
    }
}