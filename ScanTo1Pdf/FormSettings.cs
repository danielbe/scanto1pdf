﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScanTo1Pdf
{
    public partial class FormSettings : Form
    {
        public FormSettings()
        {
            InitializeComponent();
            InitializeWithCurrentValues();
        }

        private void ControlAccept_Click(object sender, EventArgs e)
        {
            Values.DeleteFiles = ControlDeleteFiles.Checked;
            
            if (!ControlImagePath.Text.Equals(""))
                Values.TempImgPath = ControlImagePath.Text;
            if (Values.TempImgPath.Equals("")) Values.TempImgPath = Values.DesktopDefaultPath;

            if (!ControlPdfPath.Text.Equals(""))
                Values.FinalPdfPath = ControlPdfPath.Text;
            if (Values.FinalPdfPath.Equals("")) Values.FinalPdfPath = Values.DesktopDefaultPath;

            if (!Values.FinalPdfPath.EndsWith("\\"))
                Values.FinalPdfPath += "\\";
            if (!Values.TempImgPath.EndsWith("\\"))
                Values.TempImgPath += "\\";

            //Values.FinalPdfPath = ControlPdfPath.Text;

            ConfigurationFile.Write();

            this.Close();
        }

        private void InitializeWithCurrentValues()
        {
            ControlDeleteFiles.Checked = Values.DeleteFiles;
            ControlPdfPath.Text = Values.FinalPdfPath;
            ControlImagePath.Text = Values.TempImgPath;
        }
    }
}
