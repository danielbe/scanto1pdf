﻿namespace ScanTo1Pdf
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ControlOutput = new System.Windows.Forms.RichTextBox();
            this.ControlScan = new System.Windows.Forms.Button();
            this.ControlPdf = new System.Windows.Forms.Button();
            this.ControlScanners = new System.Windows.Forms.ComboBox();
            this.ControlQuality = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip3 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip4 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip5 = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ControlMenuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.ControlMenuRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.ControlMenuEmptyLog = new System.Windows.Forms.ToolStripMenuItem();
            this.ControlMenuImgPath = new System.Windows.Forms.ToolStripMenuItem();
            this.ControlMenuPdfPath = new System.Windows.Forms.ToolStripMenuItem();
            this.ControlMenuExtras = new System.Windows.Forms.ToolStripMenuItem();
            this.ControlMenuSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.ControlMenuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ControlOutput
            // 
            this.ControlOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ControlOutput.HideSelection = false;
            this.ControlOutput.Location = new System.Drawing.Point(12, 83);
            this.ControlOutput.Name = "ControlOutput";
            this.ControlOutput.ReadOnly = true;
            this.ControlOutput.Size = new System.Drawing.Size(260, 170);
            this.ControlOutput.TabIndex = 0;
            this.ControlOutput.Text = "";
            // 
            // ControlScan
            // 
            this.ControlScan.Location = new System.Drawing.Point(12, 54);
            this.ControlScan.Name = "ControlScan";
            this.ControlScan.Size = new System.Drawing.Size(137, 23);
            this.ControlScan.TabIndex = 1;
            this.ControlScan.Text = "Scan";
            this.toolTip4.SetToolTip(this.ControlScan, "Scannen mit der eingestellten Qualität. Es wird ein Bild am in den Einstellungen " +
        "angegebenen Pfad zwischengespeichert");
            this.ControlScan.UseVisualStyleBackColor = true;
            this.ControlScan.Click += new System.EventHandler(this.scan_Click);
            // 
            // ControlPdf
            // 
            this.ControlPdf.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ControlPdf.Location = new System.Drawing.Point(155, 54);
            this.ControlPdf.Name = "ControlPdf";
            this.ControlPdf.Size = new System.Drawing.Size(117, 23);
            this.ControlPdf.TabIndex = 2;
            this.ControlPdf.Text = "PDF";
            this.toolTip5.SetToolTip(this.ControlPdf, "Alle gescannten Bilder zusammenfügen. Ein PDF kann Bilder unterschiedlicher Quali" +
        "täten enthalten");
            this.ControlPdf.UseVisualStyleBackColor = true;
            this.ControlPdf.Click += new System.EventHandler(this.save_Click);
            // 
            // ControlScanners
            // 
            this.ControlScanners.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ControlScanners.FormattingEnabled = true;
            this.ControlScanners.Location = new System.Drawing.Point(12, 27);
            this.ControlScanners.Name = "ControlScanners";
            this.ControlScanners.Size = new System.Drawing.Size(199, 21);
            this.ControlScanners.TabIndex = 4;
            this.toolTip2.SetToolTip(this.ControlScanners, "Alle gefundenen Scanner");
            this.ControlScanners.SelectedIndexChanged += new System.EventHandler(this.scanners_SelectedIndexChanged);
            // 
            // ControlQuality
            // 
            this.ControlQuality.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ControlQuality.FormattingEnabled = true;
            this.ControlQuality.Items.AddRange(new object[] {
            "Gut",
            "Ok",
            "Mies"});
            this.ControlQuality.Location = new System.Drawing.Point(217, 27);
            this.ControlQuality.Name = "ControlQuality";
            this.ControlQuality.Size = new System.Drawing.Size(55, 21);
            this.ControlQuality.TabIndex = 6;
            this.ControlQuality.Text = "Ok";
            this.toolTip1.SetToolTip(this.ControlQuality, "Qualität des Scans (ca. 0,1 - 1,2 MB)");
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ControlMenuFile,
            this.ControlMenuExtras});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ControlMenuFile
            // 
            this.ControlMenuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ControlMenuRefresh,
            this.ControlMenuEmptyLog,
            this.ControlMenuImgPath,
            this.ControlMenuPdfPath});
            this.ControlMenuFile.Name = "ControlMenuFile";
            this.ControlMenuFile.Size = new System.Drawing.Size(46, 20);
            this.ControlMenuFile.Text = "Datei";
            // 
            // ControlMenuRefresh
            // 
            this.ControlMenuRefresh.Name = "ControlMenuRefresh";
            this.ControlMenuRefresh.Size = new System.Drawing.Size(180, 22);
            this.ControlMenuRefresh.Text = "Neu laden";
            this.ControlMenuRefresh.Click += new System.EventHandler(this.ControlMenuRefresh_Click);
            // 
            // ControlMenuEmptyLog
            // 
            this.ControlMenuEmptyLog.Name = "ControlMenuEmptyLog";
            this.ControlMenuEmptyLog.Size = new System.Drawing.Size(180, 22);
            this.ControlMenuEmptyLog.Text = "Log leeren";
            this.ControlMenuEmptyLog.Click += new System.EventHandler(this.ControlMenuEmptyLog_Click);
            // 
            // ControlMenuImgPath
            // 
            this.ControlMenuImgPath.Name = "ControlMenuImgPath";
            this.ControlMenuImgPath.Size = new System.Drawing.Size(180, 22);
            this.ControlMenuImgPath.Text = "Bilderpfad anzeigen";
            this.ControlMenuImgPath.Click += new System.EventHandler(this.ControlMenuImgPath_Click);
            // 
            // ControlMenuPdfPath
            // 
            this.ControlMenuPdfPath.Name = "ControlMenuPdfPath";
            this.ControlMenuPdfPath.Size = new System.Drawing.Size(180, 22);
            this.ControlMenuPdfPath.Text = "Pdf-Pfad anzeigen";
            this.ControlMenuPdfPath.Click += new System.EventHandler(this.ControlMenuPdfPath_Click);
            // 
            // ControlMenuExtras
            // 
            this.ControlMenuExtras.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ControlMenuSettings,
            this.ControlMenuHelp});
            this.ControlMenuExtras.Name = "ControlMenuExtras";
            this.ControlMenuExtras.Size = new System.Drawing.Size(50, 20);
            this.ControlMenuExtras.Text = "Extras";
            // 
            // ControlMenuSettings
            // 
            this.ControlMenuSettings.Name = "ControlMenuSettings";
            this.ControlMenuSettings.Size = new System.Drawing.Size(145, 22);
            this.ControlMenuSettings.Text = "Einstellungen";
            this.ControlMenuSettings.Click += new System.EventHandler(this.ControlMenuSettings_Click);
            // 
            // ControlMenuHelp
            // 
            this.ControlMenuHelp.Name = "ControlMenuHelp";
            this.ControlMenuHelp.Size = new System.Drawing.Size(145, 22);
            this.ControlMenuHelp.Text = "Hilfe";
            this.ControlMenuHelp.Click += new System.EventHandler(this.ControlMenuHelp_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 265);
            this.Controls.Add(this.ControlQuality);
            this.Controls.Add(this.ControlScanners);
            this.Controls.Add(this.ControlPdf);
            this.Controls.Add(this.ControlScan);
            this.Controls.Add(this.ControlOutput);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Tastscharf";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox ControlOutput;
        private System.Windows.Forms.Button ControlScan;
        private System.Windows.Forms.Button ControlPdf;
        private System.Windows.Forms.ComboBox ControlScanners;
        private System.Windows.Forms.ComboBox ControlQuality;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.ToolTip toolTip3;
        private System.Windows.Forms.ToolTip toolTip4;
        private System.Windows.Forms.ToolTip toolTip5;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ControlMenuFile;
        private System.Windows.Forms.ToolStripMenuItem ControlMenuExtras;
        private System.Windows.Forms.ToolStripMenuItem ControlMenuRefresh;
        private System.Windows.Forms.ToolStripMenuItem ControlMenuSettings;
        private System.Windows.Forms.ToolStripMenuItem ControlMenuHelp;
        private System.Windows.Forms.ToolStripMenuItem ControlMenuEmptyLog;
        private System.Windows.Forms.ToolStripMenuItem ControlMenuImgPath;
        private System.Windows.Forms.ToolStripMenuItem ControlMenuPdfPath;
    }
}

