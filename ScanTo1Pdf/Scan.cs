﻿using Org.BouncyCastle.Asn1;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WIA;

namespace ScanTo1Pdf
{
    class Scan
    {
        DeviceManager deviceManager;
        Device device;
        Item scannerItem;
        readonly int quality;
        //readonly string currentScanner;

        public Scan()
        {
            deviceManager = new DeviceManager();
        }

        public Scan(int quality, string currentScanner)
        {
            deviceManager = new DeviceManager();
            //this.currentScanner = currentScanner;
            this.quality = quality;
            ConnectToScanner(currentScanner);
        }
        

        public List<string> SearchForScanner()
        {
            deviceManager = new DeviceManager();
            List<string> scannerNames = new List<string>();

            //list of devices
            for (int i = 1; i <= deviceManager.DeviceInfos.Count; i++)
            {
                //skip the device if not a scanner
                if (deviceManager.DeviceInfos[i].Type == WiaDeviceType.ScannerDeviceType)
                {
                    //add name of scanner to list
                    scannerNames.Add(deviceManager.DeviceInfos[i].Properties["Name"]
                        .get_Value().ToString());
                }
            }
            return scannerNames;
        }

        public int ConnectToScanner(string scannerToConnect)
        {
            int errorLevel = 2;

            for (int i = 1; i <= deviceManager.DeviceInfos.Count; i++)
            {
                if(deviceManager.DeviceInfos[i].Properties["Name"].get_Value().ToString()
                    .Equals(scannerToConnect))
                {
                    try
                    {
                        device = deviceManager.DeviceInfos[i].Connect();
                        scannerItem = device.Items[1];
                        errorLevel = 0;
                    }
                    catch (Exception) { errorLevel = 1; }
                }
            }
            return errorLevel;
        }

        public ScanResult GetScan()
        {
            ScanResult scanResult = new ScanResult();

            //string path = $@"C:\Users\{Help.GetUserName()}\Desktop\scan{Help.GetDateTime()}.jpeg";
            string path = Values.TempImgPath + $@"scan{Help.GetDateTime()}.jpeg";
            //string path = Values.TempImgPath + $@"\pdf{Help.GetDateTime()}.pdf"; //???????? why pdf and not jpeg
            scanResult.Path = path;

            ImageProcess ip = new ImageProcess();
            ip.Filters.Add(ip.FilterInfos["Convert"].FilterID);
            ip.Filters[1].Properties["Quality"].set_Value(quality); //1 worst, 100 best
            ip.Filters[1].Properties["FormatID"].set_Value(FormatID.wiaFormatJPEG);

            try
            {
                ImageFile ImageFile = (ImageFile)scannerItem.Transfer(FormatID.wiaFormatJPEG);
                ImageFile = ip.Apply(ImageFile);
                ImageFile.SaveFile(path);
            }
            catch (NullReferenceException)
            {
                scanResult.ErrorMessage = Texts.NoScannerSelected;
                scanResult.Success = false;
            }
            catch (DirectoryNotFoundException)
            {
                scanResult.ErrorMessage = Texts.CantAccessPath + Environment.NewLine +
                    path + Environment.NewLine + Texts.PleaseChangePath;
                scanResult.Success = false;
            }
            catch (Exception ex)
            {
                scanResult.ErrorMessage = ex.Message;
                scanResult.Success = false;
            }
            return scanResult;
        }
    }
}
