﻿using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Möglichkeit der Angabe eines Pfades

namespace ScanTo1Pdf
{
    public partial class Form1 : Form
    {
        public List<string> JpegFiles = new List<string>();
        public List<string> Scanners = new List<string>();
        Scan scan = new Scan();

        public Form1(string[] args)
        {
            InitializeComponent();

            if (!File.Exists(Values.ConfigurationFile))
                Output(ConfigurationFile.Create());
            ConfigurationFile.Read();

            if (args.Length > 0) //Args as Pdf
            {
                SaveAsPdf save = new SaveAsPdf(args.ToList(), false);
                string output = "";

                try
                {
                    output = save.SaveImgAsPdf();
                }
                catch(Exception ex)
                {
                    output = Texts.ErrorCreatingPdf + ": " +  ex.Message;
                }
                //else
                //    output = save.SaveDocAsPdf();
                Output(output);
            }

            loadScanners();

            if (!File.Exists(Values.ConfigurationFile))
            {
                try { Help.ResetSettingsFile(); }
                catch (IOException) { Output(Texts.ConfiguationFileError); }
            }
        }
        
        private void scan_Click(object sender, EventArgs e)
        {
            Output(Texts.Scanning);

            Scan scQ = new Scan(getQuality(), getCurrentScanner());
            ScanResult scanResult = scQ.GetScan();
            if (scanResult.Success)
            {
                JpegFiles.Add(scanResult.Path);
                Output(Texts.SaveScanToPath);
            }
            else
            {
                Output(scanResult.ErrorMessage);
            }
        }

        //gets jpegs in list and puts them together in pdf, then deletes files and empties list
        private void save_Click(object sender, EventArgs e)
        {
            SaveAsPdf sp = new SaveAsPdf(JpegFiles, true);
            if (JpegFiles.Count > 0)
            {
                Output(Texts.CreatingPdf);
                Output(sp.SaveImgAsPdf());
            }
            else
            {
                Output(Texts.NoScanToCreatePdf);
            }
            JpegFiles.Clear();
        }

        public void Output(string text)
        {
            ControlOutput.AppendText(text);
            ControlOutput.AppendText(Environment.NewLine);
        }
        
        private void loadScanners()
        {
            ControlScanners.Items.Clear();
            ControlScanners.ResetText();
            Scanners = scan.SearchForScanner();
            foreach (var s in Scanners)
            {
                Output($"{Texts.FoundScanners}: {s}");
                ControlScanners.Items.Add(s);
                ControlScanners.SelectedItem = s;
            }
        }

        private void scanners_SelectedIndexChanged(object sender, EventArgs e)
        {
            string currentItem = getCurrentScanner();

            if (!String.IsNullOrEmpty(currentItem))
            {
                int error = scan.ConnectToScanner(currentItem);
                if (error.Equals(0))
                    Output($"{Texts.ScannerConnectionSuccess} {currentItem}");
                else if (error.Equals(1))
                    Output($"{Texts.ScannerConnectionFailure} {currentItem}");
                else if (error.Equals(2))
                    Output($"{currentItem} {Texts.ScannerConnectionLost}");
                else
                    Output(Texts.UnknownError);
            }
            else
            {
                Output(Texts.NoScannerSelected);
            }
        }

        private int getQuality()
        {
            string qualityText = ControlQuality.Text.ToLower();

            int qu = 50;
            if (qualityText.Equals("gut"))
                qu = 80;
            else if (qualityText.Equals("mies"))
                qu = 10;
            else if (qualityText.Equals("scheiße"))
                qu = 1;
            else if (!qualityText.Equals("ok"))
                Output(Texts.SelectedQualityNotAvailable);

            return qu;
        }

        private string getCurrentScanner()
        {
            var currentScanner = ControlScanners.SelectedItem;
            return currentScanner != null ? currentScanner.ToString() : "";
        }

        private void ControlMenuRefresh_Click(object sender, EventArgs e)
        {
            loadScanners();
            Output(Texts.Refreshed);
        }

        private void ControlMenuHelp_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Texts.PdfArgs + Environment.NewLine
                + Texts.NoScanner, "Hilfe");
        }

        private void ControlMenuSettings_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Einstellungen noch nicht verfügbar", "Einstellungen noch nicht verfügbar");
            new FormSettings().Show();
        }

        private void ControlMenuEmptyLog_Click(object sender, EventArgs e)
        {
            ControlOutput.Text = "";
        }

        private void ControlMenuImgPath_Click(object sender, EventArgs e)
        {
            NavigateToPath(Values.TempImgPath);
        }

        private void ControlMenuPdfPath_Click(object sender, EventArgs e)
        {
            NavigateToPath(Values.FinalPdfPath);
        }

        private void NavigateToPath(string path)
        {
            try
            {
                Process.Start(path);
            }
            catch (Exception ex)
            {
                Output($"{ex.Message}{Environment.NewLine}{Texts.UsedPath} {path}");
            }
        }
    }
}
