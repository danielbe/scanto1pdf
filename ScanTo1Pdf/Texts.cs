﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScanTo1Pdf
{
    public static class Texts
    {
        #region Scanner
        public const string NoScannerSelected =
            "Bitte einen Scanner aus obiger Liste auswählen. " +
            "Ist kein Scanner aufgeführt, den Scanner mit dem PC " +
            "verbinden und im Menü oben neu laden klicken.";

        public const string ScannerConnectionSuccess =
            "Erfolgreich verbunden mit";

        public const string ScannerConnectionFailure =
            "Fehler beim Verbinden mit";

        public const string ScannerConnectionLost =
            "kann nicht mehr gefunden werden. Neu laden empfohlen";

        public const string FoundScanners =
            "Gefundene Scanner";

        public const string SaveScanToPath =
            "Der Scan wurde zwischengespeichert";

        public const string Scanning =
            "Scannt gerade..";

        public const string Refreshed = 
            "Es wurde nach neuen Scannern gesucht";
        #endregion
        #region Pdf
        public const string CreatingPdf =
            "PDF wird erstellt..";

        public const string NoScanToCreatePdf =
            "Das PDF kann erst erstellt werden, wenn mindestens " +
            "ein Scan vorhanden ist";

        public const string ErrorCreatingPdf =
            "Allgemeiner Fehler beim Erstellen des PDFs";
        #endregion
        #region Help
        public const string PdfArgs = "Bilder, die dem Programm als Argumente "
            + "mitgegeben wurden, werden als PDF gespeichert. " +
            "Dazu einfach eine oder mehrere Dateien im Windows Explorer " +
            "markieren und per Drag & Drop auf das Programm ziehen";
        public const string NoScanner = "Wird kein Scanner angezeigt, stellen "
            + "Sie sicher, dass Windows ihn erkannt hat";
        #endregion
        #region Path
        public const string CantAccessPath = "Auf in den Einstellungen festgelegten " +
            "Pfad kann nicht zugegriffen werden:";
        public const string PleaseChangePath = 
            "Bitte den Pfad in den Einstellungen (oben) ändern";
        public const string UsedPath = "Verwendeter Pfad:";
        #endregion

        public const string SelectedQualityNotAvailable =
            "Angegebene Qualität konnte nicht verwertet werden. " +
            "Standardqualität wird verwendet";

        public const string UnknownError =
            "Unbekannter Fehler. Bitte das nicht vorhandene Handbuch lesen";

        public const string SaveSuccess =
            "Erfolgreich abgespeichert";

        public const string ConfiguationFileError = "Konfigurationsdatei " +
            "konnte nicht erstellt werden.";

    }
}
