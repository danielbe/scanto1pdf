﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using Microsoft.Office.Interop.Word;

namespace ScanTo1Pdf
{
    class SaveAsPdf
    {
        List<string> files;
        bool filesFromScan;

        public SaveAsPdf(List<string> files, bool filesFromScan)
        {
            this.files = files;
            this.filesFromScan = filesFromScan;
        }

        public string SaveImgAsPdf()
        {
            //string path = $@"C:\Users\{Help.GetUserName()}\Desktop\pdf{Help.GetDateTime()}.pdf";
            string path = Values.FinalPdfPath + $@"pdf{Help.GetDateTime()}.pdf";
            iTextSharp.text.Rectangle pageSize = null;
            string output = "";

            files.RemoveAll(f => !File.Exists(f));

            if (files.Count >= 1 )
            {
                using (var srcImage = new Bitmap(files.First()))
                {
                    pageSize = new iTextSharp.text.Rectangle(0, 0, srcImage.Width, srcImage.Height);
                }
                using (var ms = new MemoryStream())
                {
                    using (var document = new iTextSharp.text.Document(pageSize, 0, 0, 0, 0))
                    {
                        iTextSharp.text.pdf.PdfWriter.GetInstance(document, ms).SetFullCompression();
                        document.Open();
                        foreach (var f in files)
                        {
                            if (File.Exists(f))
                            {
                                try
                                {
                                    document.Add(iTextSharp.text.Image.GetInstance(f));
                                }
                                catch(Exception)
                                {
                                    output += $"Datei {f} wurde überprungen! " + Environment.NewLine;
                                }
                            }
                                
                        }
                        //document.Close();
                    }
                    File.WriteAllBytes(path, ms.ToArray());
                }

                //Only delete if files were generated through this program and deleteFiles true
                if (filesFromScan && Values.DeleteFiles)
                    files.ForEach(f => File.Delete(f));
                //foreach (var f in JpegFiles)
                //{
                //    File.Delete(f);
                //}

                files.Clear();
                output += $"{Texts.SaveSuccess}: {Environment.NewLine}{path}";
            }
            return output;
        }

        public string SaveDocAsPdf()
        {
            //https://stackoverflow.com/questions/607669/how-do-i-convert-word-files-to-pdf-programmatically

            Application appWord = new Application();
            Document wordDocument;
            string output = "";
            foreach (string file in files)
            {
                wordDocument = appWord.Documents.Open(file);
                string path = Values.FinalPdfPath + $@"pdf{Help.GetDateTime()}.pdf";
                wordDocument.ExportAsFixedFormat(path, WdExportFormat.wdExportFormatPDF);
                output = $"{Texts.SaveSuccess}: {Environment.NewLine}{path}";
            }
            return output;
        }
    }
}
